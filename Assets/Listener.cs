﻿using UnityEngine;
using System.Collections;

public class Listener : MonoBehaviour {

    private GameObject triggeredWith = null;
    private GameObject collidedWith = null;

    void OnCollisionEnter2D(Collision2D coll)
    {
        collidedWith = coll.gameObject;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        triggeredWith = coll.gameObject;
    }
	
	
	void FixedUpdate () {
        if (triggeredWith != null && collidedWith == null)
        {
            Debug.LogError(gameObject.name + " triggered with " + triggeredWith.name + " but no OnCollisionEnter2D called");
        }

        if (triggeredWith == null && collidedWith != null)
        {
            Debug.LogError(gameObject.name + " collided with " + collidedWith.name + " but no OnTriggerEnter2D called");
        }

        triggeredWith = collidedWith = null;
	}
}
