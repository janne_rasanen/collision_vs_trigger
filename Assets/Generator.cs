﻿using UnityEngine;
using System.Collections;

public class Generator : MonoBehaviour {
    public GameObject toBeGenerated;
    public float secondsBetweenGeneration;

    float sinceLastGeneration = 0;
    static float counter = 0;

	void Update () {
        sinceLastGeneration += Time.deltaTime;

        if (sinceLastGeneration > secondsBetweenGeneration)
        {
            Object o = Instantiate(toBeGenerated, transform.position, Quaternion.identity);
            o.name = "ball " + counter++;
            sinceLastGeneration -= secondsBetweenGeneration;
        }
	}
}
